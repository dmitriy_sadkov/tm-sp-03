package ru.sadkov.tm.service.impl;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sadkov.tm.model.User;
import ru.sadkov.tm.model.enumerate.Role;
import ru.sadkov.tm.repository.UserRepository;
import ru.sadkov.tm.service.IUserService;
import ru.sadkov.tm.util.HashUtil;
import ru.sadkov.tm.util.RandomUtil;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserService extends AbstractService implements IUserService {

    @Autowired
    @NotNull
    private UserRepository userRepository;

    @Autowired
    @NotNull
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public @NotNull User createUser(String login, String password) {
        @NotNull final User user = new User();
        user.setId(RandomUtil.UUID());
        user.setLogin(login);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        user.setRole(Role.USER);
        user.setProjectList(new ArrayList<>());
        return user;
    }

    @Override
    @Transactional
    public void userRegister(@Nullable final User user) {
        if (user == null) return;
        if (user.getLogin() == null || user.getLogin().isEmpty()) return;
        if (user.getPassword() == null || user.getPassword().isEmpty()) return;
        userRepository.save(user);
    }

    @Transactional
    public void userAddFromData(@Nullable final User user) {
        if (user == null) return;
        userRepository.save(user);
    }


    @Override
    @Transactional
    public void userRegister(@Nullable String login, @Nullable String password) {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        @NotNull final User user = new User(login, password, Role.USER);
        user.setId(RandomUtil.UUID());
        user.setPassword(HashUtil.hashMD5(user.getPassword()));
        userRepository.save(user);
    }

    public boolean login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) return false;
        if (password == null || password.isEmpty()) return false;
        if (userRepository.existsByLogin(login)) {
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null || user.getPassword() == null) return false;
            return user.getPassword().equals(HashUtil.hashMD5(password));
        }
        return false;
    }

    @Override
    public @NotNull List<User> findAll() {
        return ((List<User>) (userRepository.findAll()));

    }

    public void addTestUsers() {
        @NotNull final User admin = new User("admin", "admin", Role.ADMIN);
        @NotNull final User user = new User("user", "user", Role.USER);
        userRegister(admin);
        userRegister(user);
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

    @Override
    public void load(@Nullable final List<User> users) {
        if (users == null) return;
        clear();
        for (@NotNull final User user : users) {
            userRegister(user);
        }
    }

    @Override
    @Nullable
    public User findOneById(@NotNull String userId) {
        if (userRepository.findById(userId).isPresent()) return userRepository.findById(userId).get();
        return null;
    }

    @Override
    @Nullable
    public User findOneByLogin(@Nullable final String login) {
        return userRepository.findByLogin(login);
    }
}
