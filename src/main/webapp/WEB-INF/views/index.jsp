<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!doctype html>
<html lang="en">
<head>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Spring MVC</title>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar navbar-dark bg-primary">
    <a class="navbar-brand" href="${pageContext.request.contextPath}">Project Manager</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <sec:authorize access="isAuthenticated()">
                    <a class="nav-link" href="${pageContext.request.contextPath}/projects/<sec:authentication property="principal.username"/>">Projects<span class="sr-only">(current)</span></a>
                </sec:authorize>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/developer">Developer<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item active">
                <sec:authorize access="!isAuthenticated()">
                    <a class="nav-link" href="${pageContext.request.contextPath}/login">Login<span class="sr-only">(current)</span></a>
                </sec:authorize>
            </li>
            <li class="nav-item active">
                <sec:authorize access="isAuthenticated()">
                    <a class="nav-link" href="${pageContext.request.contextPath}/logout">Logout<span class="sr-only">(current)</span></a>
                </sec:authorize>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="${pageContext.request.contextPath}/registration">Registration<span
                        class="sr-only">(current)</span></a>
            </li>
        </ul>
        <div class="ml-auto p-2 bd-highlight">
            <sec:authorize access="isAuthenticated()">
                <a class="btn btn-primary"
                   href="${pageContext.request.contextPath}/profile/<sec:authentication property="principal.username"/>">Hello!
                    <sec:authentication property="principal.username"/><span
                            class="sr-only">(current)</span></a>
            </sec:authorize>
        </div>
    </div>
</nav>
<div class="container-fluid">
    <sec:authorize access="!isAuthenticated()">
        <h1>Hello Guest!!</h1>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
    <h1>Hello! <sec:authentication property="principal.username"/></h1>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/projects/<sec:authentication property="principal.username"/>">Show Projects</a>
    </sec:authorize>
    <sec:authorize access="!isAuthenticated()">
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/login">Login</a>
        <a class="btn btn-primary" href="${pageContext.request.contextPath}/registration">Registration</a>
    </sec:authorize>
</div>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>